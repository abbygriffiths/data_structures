#pragma once

#include <memory>
#include <vector>

namespace data_structures
{
  template <typename T> class node;

  template <typename T> class stack
  {
  private:
    std::unique_ptr<node<T>> top;

  public:
    stack();

    stack<T>& push(const node<T>& item);
    node<T> pop();

    bool is_empty() const;

    std::vector<T> traverse() const;
  };
}
