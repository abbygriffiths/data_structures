#pragma once

#include <memory>

namespace data_structures
{
  template <typename T> class node
  {
  private:
    T value;
    std::unique_ptr<T> next;

  public:
    node();
    node(const T& value_);
    node(const node<T>& n2);

    node<T>& set_next(const std::unique_ptr<T>& next_);
    std::unique_ptr<T> get_next() const;

    template <typename S>
    friend std::ostream& operator<<(std::ostream& os, const node<S>& n);
  };
}
