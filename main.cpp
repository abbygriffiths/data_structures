#include <algorithm>
#include <iostream>

#include "node.hpp"
#include "stack.hpp"

using namespace data_structures;

template <typename T>
void print_array(const std::vector<T>& arr);

int main()
{
  stack<int> st;
  std::cout << "Pushing 5" << std::endl;
  st.push(node(5));

  auto st_arr = st.traverse();
  print_array(st_arr);

  std::cout << "Pushing 10" << std::endl;
  st.push(node(10));

  st_arr = st.traverse();
  print_array(st_arr);

  std::cout << "Popping value: " << st.pop() << std::endl;
  st_arr = st.traverse();
  print_array(st_arr);

  return 0;
}

template <typename T>
void print_array(const std::vector<T>& arr)
{
  std::for_each(std::begin(arr), std::end(arr),
                [](const auto& n)
                {
                  std::cout << n << std::endl;
                });
}
