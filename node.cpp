#include "node.hpp"

namespace data_structures
{
  template <typename T>
  node<T>::node():
    value(T{}),
    next(nullptr)
  {
  }

  template <typename T>
  node<T>::node(const node<T>& n2):
    value(n2.value),
    next(n2.next)
  {
  }

  template <typename T>
  node<T>::node(const T& value_):
    value(value_),
    next(nullptr)
  {
  }

  template <typename T>
  node<T>& node<T>::set_next(const std::unique_ptr<T>& next_)
  {
    next = next_;
    return *this;
  }

  template <typename T>
  std::unique_ptr<T> node<T>::get_next() const
  {
    return next;
  }

  template <typename S>
  std::ostream& operator<<(std::ostream& os, const node<S>& n)
  {
    os << n.value;
    return os;
  }
}
