#include "node.hpp"
#include "stack.hpp"

namespace data_structures
{
  template <typename T>
  stack<T>::stack():
    top(nullptr)
  {
  }

  template <typename T>
  stack<T>& stack<T>::push(const node<T>& item)
  {
    if (is_empty())
      {
        top = std::make_unique<T>(item);
      }
    else
      {
        node<T> temp(item);
        temp.set_next(top);
        top = temp;
      }

    return *this;
  }

  template <typename T>
  node<T> stack<T>::pop()
  {
    if (! is_empty())
      {
        node<T> temp(*top);
        top = top->get_next();
        return temp;
      }
    else
      {
        throw std::exception("Cannot pop from empty stack!");
      }
  }

  template <typename T>
  bool stack<T>::is_empty() const
  {
    return top == nullptr;
  }

  template <typename T>
  std::vector<T> stack<T>::traverse() const
  {
    std::vector<T> arr {};

    for (node<T> n = *top; top->get_next(); n = *(top->get_next()))
      {
        arr.push_back(n);
      }
    return arr;
  }
}
